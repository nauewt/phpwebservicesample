OAuth 2.0 sample app
============================

This sample app uses an OAuth 2.0 client to show off NAU's new Enterprise 
Web Services.

CURRENT END POINTS
============================
https://dev-oleadmin.ucc.nau.edu/api/me
https://dev-oleadmin.ucc.nau.edu/api/me/addresses
https://dev-oleadmin.ucc.nau.edu/api/me/names
https://dev-oleadmin.ucc.nau.edu/api/me/emails
https://dev-oleadmin.ucc.nau.edu/api/me/phones
https://dev-oleadmin.ucc.nau.edu/api/me/gender
https://dev-oleadmin.ucc.nau.edu/api/me/ethnicity
https://dev-oleadmin.ucc.nau.edu/api/me/gps
https://dev-oleadmin.ucc.nau.edu/api/me/major
https://dev-oleadmin.ucc.nau.edu/api/me/finaid/YEAR

INSTALLATION
============================
Update the following to include your own PUBLIC.KEY, PRIVATE.KEY and Callback.

	:::php
		// configuration of client credentials
		$client = new OAuth2\Client(
		        'PUBLIC.KEY',
		        'PRIVATE.KEY',
		        'http://callback/endpoint.php');
