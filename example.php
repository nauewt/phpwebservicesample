<?php

spl_autoload_register(function ($class) {
    require str_replace('\\', DIRECTORY_SEPARATOR, $class) . '.php';
});

// configuration of client credentials
$client = new OAuth2\Client(
        'PUBLIC.KEY',
        'PRIVATE.KEY',
        'http://callback/endpoint.php');

// configuration of service
$configuration = new OAuth2\Service\Configuration(
        'https://dev-oleadmin.ucc.nau.edu/api/oauth2/auth',
        'https://dev-oleadmin.ucc.nau.edu/api/oauth2/token');

// storage class for access token, just implement OAuth2\DataStore interface for
// your own implementation
$dataStore = new OAuth2\DataStore\Session();

$scope = 'bio';

$service = new OAuth2\Service($client, $configuration, $dataStore, $scope);

if (isset($_GET['action'])) {
    switch ($_GET['action']) {
        case 'authorize':
            // redirects to authorize endpoint
            $service->authorize();
            break;
        case 'requestAddresses':
            // calls api endpoint with access token
            $webServiceResult = $service->callApiEndpoint('https://dev-oleadmin.ucc.nau.edu/api/me/addresses');
            break;
    case 'requestNames':
        $webServiceResult = $service->callApiEndpoint('https://dev-oleadmin.ucc.nau.edu/api/me/names');
        break;
    case 'requestEmails':
        $webServiceResult = $service->callApiEndpoint('https://dev-oleadmin.ucc.nau.edu/api/me/emails');
        break;
    case 'requestPhones':
        $webServiceResult = $service->callApiEndpoint('https://dev-oleadmin.ucc.nau.edu/api/me/phones');
        break;
    case 'requestMe':
        $webServiceResult = $service->callApiEndpoint('https://dev-oleadmin.ucc.nau.edu/api/me/');
        break;
    case 'requestGender':
        $webServiceResult = $service->callApiEndpoint('https://dev-oleadmin.ucc.nau.edu/api/me/gender');
        break;
    case 'requestEthnicity':
        $webServiceResult = $service->callApiEndpoint('https://dev-oleadmin.ucc.nau.edu/api/me/ethnicity');
        break;
    case 'requestGpas':
        $webServiceResult = $service->callApiEndpoint('https://dev-oleadmin.ucc.nau.edu/api/me/gpa');
        break;
    case 'requestMajor':
        $webServiceResult = $service->callApiEndpoint('https://dev-oleadmin.ucc.nau.edu/api/me/major');
        break;
    case 'requestFinAid':
                $webServiceResult = $service->callApiEndpoint('https://dev-oleadmin.ucc.nau.edu/api/me/finaid/2013');
                break;
    }
}

if (isset($_GET['code'])) {
    // retrieve access token from endpoint
    $service->getAccessToken();
}

$token = $dataStore->retrieveAccessToken();

?>
<html>
    <head>
    </head>
    <body>
        Access Token: <input type="text" id="access-token" value="<?php echo $token->getAccessToken() ?>" /><br />
        Refresh Token: <input type="text" id="refresh-token" value="<?php echo $token->getRefreshToken() ?>" /><br />
        LifeTime: <input type="text" id="lifetime" value="<?php echo $token->getLifeTime() ?>" /><br />
        <br />
        <a href="example.php?action=authorize" id="authorize">authorize</a><br />
        <br />
    <a href="example.php?action=requestMe" id="request-api">request Me(Summary of everything)(https://dev-oleadmin.ucc.nau.edu/api/me)</a><br />
    <br/>
        <a href="example.php?action=requestAddresses" id="request-api">request Addresses(https://dev-oleadmin.ucc.nau.edu/api/me/addresses) </a><br />
    <a href="example.php?action=requestNames" id="request-api">request Names(https://dev-oleadmin.ucc.nau.edu/api/me/names)</a><br />
    <a href="example.php?action=requestEmails" id="request-api">request Emails(https://dev-oleadmin.ucc.nau.edu/api/me/emails)</a><br />
    <a href="example.php?action=requestPhones" id="request-api">request Phones(https://dev-oleadmin.ucc.nau.edu/api/me/phones)</a><br />
    <a href="example.php?action=requestGender" id="request-api">request Gender(https://dev-oleadmin.ucc.nau.edu/api/me/gender)</a><br />
    <a href="example.php?action=requestEthnicity" id="request-api">request Ethnicity(https://dev-oleadmin.ucc.nau.edu/api/me/ethnicity)</a><br />
    <a href="example.php?action=requestGpas" id="request-api">request GPAs(https://dev-oleadmin.ucc.nau.edu/api/me/gps)</a><br />
    <a href="example.php?action=requestMajor" id="request-api">request Major(https://dev-oleadmin.ucc.nau.edu/api/me/major)</a><br />
    <a href="example.php?action=requestFinAid" id="request-finaid">request Finaid(https://dev-oleadmin.ucc.nau.edu/api/me/finaid/2013)</a><br/>

        <?php if(isset($webServiceResult)){ ?>
            <h2>Result of API call</h2>
            <code><?php echo $webServiceResult ?></code>
        <?php }?>
    </body>
</html>
